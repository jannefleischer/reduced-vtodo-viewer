<?php
/**
 * Plugin Name:       reduced-vtodo-viewer
 * Plugin URI:        
 * Description:       Plugin provides a shortcode-functionality to show all VTODO entries in a linked ical file. Very basic.
 * Version:           0.1.1
 * Requires at least: 5.7
 * Author:            Janne Jakob Fleischer
 * Author URI:        https://portfolio.jannefleischer.de
 * License:			  MIT
 */

require_once 'src/reduced-vtodo-viewer.php';

