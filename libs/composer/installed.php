<?php return array(
    'root' => array(
        'name' => 'jannefleischer/reduced-vtodo-viewer',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'jannefleischer/reduced-vtodo-viewer' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'johngrogg/ics-parser' => array(
            'pretty_version' => 'v3.2.1',
            'version' => '3.2.1.0',
            'reference' => 'a3a291c51c378cc4a7f3aec6cd2478bdbd110575',
            'type' => 'library',
            'install_path' => __DIR__ . '/../johngrogg/ics-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
