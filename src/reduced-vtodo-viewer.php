<?php
 
namespace Jannefleischer\ReducedVtodoViewer;

require_once(__DIR__.'/../libs/autoload.php');
use ICal\ICal;

if ( ! defined( 'ABSPATH' ) ) {
    die( '' );
}

class ReducedVtodoViewer{
	
	public function init_shortcode() {
		add_shortcode('vtodo-viewer', array($this,'shortcode_handler'));
	}

    public function shortcode_handler($attributes, $content, $tag) {
		
		extract( shortcode_atts( array(
			'url' => '',
		), $attributes ) );
		
		$vtodos = $this->getICal($url);
		$listEl = $this->to_list($vtodos);
		return($listEl);
    }
			
    public function getICal($url) {
		try {
			$ical = new ICal($url, array(
				'defaultSpan'                 => 2,     // Default value
				'defaultTimeZone'             => 'UTC+1',
				'defaultWeekStart'            => 'MO',  // Default value
				'disableCharacterReplacement' => false, // Default value
				'filterDaysAfter'             => null,  // Default value
				'filterDaysBefore'            => null,  // Default value
				'httpUserAgent'               => null,  // Default value
				'skipRecurrence'              => true, // Default value
			));
		} catch (\Exception $e) {
			die($e);
		}
		$vtodos = $ical->cal['VTODO'];
		return $vtodos;
		//var_dump($vtodos);
    }
	
	public function to_list($vtodos) {
		$out = '
<div class="vtodo-viewer-div">
	<ul class="vtodo-viewer-ul">';
		foreach($vtodos as $key=>$value) {
		    //printf(var_dump($value));
		    if (array_key_exists('COMPLETED',$value)) {
    			$out .= '<li class="vtodo-id">'.$value['SUMMARY'].'</li>';
		    }
		}
		$out .= '
	</ul>
</div>';
		return $out;
	}
}


add_action('init', function() {
	$todos = new ReducedVtodoViewer();
	$todos->init_shortcode();
});


